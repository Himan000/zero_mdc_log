# zero_mdc_log

#### Description
zerolog with MDC。在[overlog](https://github.com/Trendyol/overlog)的基础上进行封装。

#### 使用

可以参考[otel_zero](https://gitee.com/Himan000/otel_zero.git)的使用方法。
